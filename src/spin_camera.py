#!/usr/bin/env python

import numpy as np
import rospy
import tf2_ros
import tf.transformations as tftf
from geometry_msgs.msg import TransformStamped

from synthcam.camera_util import create_tf_from_matrix44


def main_loop():
    rospy.init_node('synthcam_semantics_node')

    rate = rospy.Rate(5.)

    br = tf2_ros.TransformBroadcaster()

    T0 = tftf.translation_matrix([0., 0., 40.])
    #R0 = tftf.rotation_matrix(np.radians(-25.), [0., 1., 0.])
    # note extra 180d
    R0 = tftf.euler_matrix(np.radians(45.+180.), 0., np.radians(-55.))
    #R0 = tftf.rotation_matrix(np.radians(0), [0., 1., 0.])
    V0 = np.dot(T0, R0)

    theta = 0.
    x = 0.;
    while not rospy.is_shutdown():
        R = tftf.euler_matrix(np.radians(45.+180.), 0., np.radians(theta-180))
        # R =  tftf.rotation_matrix(np.radians(theta), [0., 1., 0.], [0., 0., 40.])
        theta = (theta+1)%360

        V = np.dot(T0, R)
        tf_msg = create_tf_from_matrix44(V, 'synthcam_model', 'synthcam_camera')
        # tf_msg = create_tf_from_matrix44(V0, 'synthcam_model', 'synthcam_camera')
        #print V

        tf_msg.header.stamp = rospy.Time.now()
        br.sendTransform(tf_msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        main_loop()
    except rospy.ROSInterruptException:
        pass
