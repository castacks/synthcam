#!/usr/bin/env python

import os
import sys
import cv2
import numpy as np
import rospy
import rospkg
import tf.transformations as tftf
import cv_bridge
import tf2_ros
from sensor_msgs.msg import Image, CameraInfo
from visualization_msgs.msg import Marker
from semantic_segmentation_msgs.msg import LabelsTensor
import cyrasterize

from spath import Path

from synthcam.camera_util import (homogenize,
                                  dehomogenize,
                                  gl_proj_matrix,
                                  create_matrix44_from_tf,
                                  CvIntrinsics)
from synthcam.mesh_util import (build_colored_scene,
                                make_mesh_marker)
from synthcam.io_util import ndarray_to_multiarray
from synthcam.img_util import (rgb_image_to_label_image,
                               rgb_image_to_onehot_image)

rospack = rospkg.RosPack()
shader_path = Path(rospack.get_path('synthcam'))/'shaders'
vertex_shader = (shader_path/'texture_shader.vert').bytes()
frag_shader = (shader_path/'texture_shader.frag').bytes()

class Node(object):

    def __init__(self):
        rospy.init_node('synthcam_semantics_node')
        self.configure()
        self.tf_buf = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tf_buf)

        self.mesh_pub = rospy.Publisher('synthcam/mesh',
                                         Marker,
                                         queue_size=3)

        self.viz_pub = rospy.Publisher('synthcam/labels_viz',
                                       Image,
                                       queue_size=3)

        self.labels_pub = rospy.Publisher('synthcam/labels',
                                           LabelsTensor,
                                           queue_size=3)

        self.viz_info_pub = rospy.Publisher('synthcam/camera_info',
                                            CameraInfo,
                                            queue_size=3)

        self.rate = rospy.Rate(self.cfg['render_rate_hz'])

        self.mesh, self.color_img, self.rgb_to_label = build_colored_scene(self.cfg['mesh_base_dir'],
                                                                           self.cfg['mesh_fnames'],
                                                                           self.cfg['mesh_colors'],
                                                                           self.cfg['mesh_classify'])
        #self.num_classes = len(self.cfg['mesh_fnames'])
        self.num_classes = len(self.rgb_to_label)

        self.cyr = cyrasterize.CyRasterizer(width=self.cfg['width'], height=self.cfg['height'])
        self.cyr.set_shaders(vertex=vertex_shader, fragment=frag_shader)
        self.cyr._opengl.set_clear_color(np.array((0,0,0,1), dtype='f4'))

        if self.cfg['intrinsics_mode']=='opengl':
            self.P = gl_proj_matrix(self.cfg['fovy_deg'], self.cfg['aspect_ratio'], self.cfg['znear'], self.cfg['zfar'])
        elif self.cfg['intrinsics_mode']=='opencv':
            self.cv_intrinsics = CvIntrinsics(self.cfg)
            self.P = self.cv_intrinsics.get_scaled_proj_matrix()
        else:
            raise ValueError('unknown intrinsics_mode')
        self.cyr.set_projection_matrix(np.ascontiguousarray(self.P))


    def configure(self):
        cfg = {}
        cfg['width'] = rospy.get_param('~width')
        cfg['height'] = rospy.get_param('~height')
        cfg['mesh_fnames'] = rospy.get_param('~mesh_fnames')
        cfg['mesh_colors'] = rospy.get_param('~mesh_colors')
        cfg['mesh_classify'] = rospy.get_param('~mesh_classify')
        cfg['mesh_base_dir'] = rospy.get_param('~mesh_base_dir')
        cfg['znear'] = rospy.get_param('~znear')
        cfg['zfar'] = rospy.get_param('~zfar')
        cfg['render_rate_hz'] = rospy.get_param('~render_rate_hz')
        cfg['publish_mesh'] = rospy.get_param('~publish_mesh')
        cfg['publish_viz'] = rospy.get_param('~publish_viz')
        cfg['publish_labels'] = rospy.get_param('~publish_labels')

        cfg['aspect_ratio'] = rospy.get_param('~aspect_ratio')

        cfg['intrinsics_mode'] = rospy.get_param('~intrinsics_mode')
        if cfg['intrinsics_mode'] == 'opencv':
            cfg['fx'] = rospy.get_param('~fx')
            cfg['fy'] = rospy.get_param('~fy')
            cfg['cx'] = rospy.get_param('~cx')
            cfg['cy'] = rospy.get_param('~cy')
            cfg['calib_width'] = rospy.get_param('~calib_width')
            cfg['calib_height'] = rospy.get_param('~calib_height')
            cfg['y_up'] = rospy.get_param('~y_up')
        elif cfg['intrinsics_mode'] == 'opengl':
            cfg['fovy_deg'] = rospy.get_param('~fovy_deg')
        else:
            raise ValueError('unknown intrinsics_mode')

        assert(len(cfg['mesh_fnames'])==len(cfg['mesh_colors']))
        self.cfg = cfg


    def get_view_matrix(self):
        try:
            # (target, source)/(parent, child)
            tf_msg = self.tf_buf.lookup_transform('synthcam_camera', 'synthcam_model', rospy.Time())
            V = create_matrix44_from_tf(tf_msg)
            # rotate 180 deg around x-axis for opencv/opengl mismatch
            # this negates y and z
            # V = np.dot(V, tftf.rotation_matrix(np.radians(180.), [1., 0., 0.]))
            V = np.dot(tftf.rotation_matrix(np.radians(180.), [1., 0., 0.]), V)

        except (tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException) as ex:
            rospy.logwarn('error reading tf: {}'.format(ex))
            return None
        return V


    def publish_img(self, rgb_img, float_img, mask):
        rgb_img = (rgb_img*255).clip(0, 255).astype('u1')
        bridge = cv_bridge.CvBridge()
        now = rospy.Time.now()
        frame_id = 'synthcam_camera'
        camera_info = self.cv_intrinsics.get_scaled_camera_info()
        # TODO support opengl mode again

        if self.cfg['publish_viz'] or self.cfg['publish_labels']:
            camera_info.header.frame_id = frame_id
            camera_info.header.stamp = now
            self.viz_info_pub.publish(camera_info)


        if self.cfg['publish_viz']:
            img_msg = bridge.cv2_to_imgmsg(cv2.cvtColor(rgb_img, cv2.COLOR_RGB2BGR))
            img_msg.header.frame_id = frame_id
            img_msg.header.stamp = now
            self.viz_pub.publish(img_msg)


        if self.cfg['publish_labels']:
            limg = rgb_image_to_onehot_image(rgb_img, self.rgb_to_label, self.num_classes)
            limg2 = (255*limg).astype('u1')
            # invert semantics of first class; 'unknown' to 'valid'
            limg2[0] = 255 - limg2[0]
            # rospy.loginfo(limg2.strides)
            labels_arr = ndarray_to_multiarray(limg2, labels=['class', 'rows', 'columns'])

            tmsg = LabelsTensor()
            tmsg.header.frame_id = frame_id
            tmsg.header.stamp = now
            tmsg.compression = 'none'
            tmsg.classes = ['class%d'%i for i in range(self.num_classes)]
            # valid = np.ones((1,)+limg.shape, dtype=yhat.dtype)*255
            tmsg.camera_info = camera_info
            tmsg.labels = labels_arr
            self.labels_pub.publish(tmsg)


    def publish_mesh(self):
        if self.cfg['publish_mesh']:
            mesh_marker = make_mesh_marker(self.mesh)
            mesh_marker.header.stamp = rospy.Time.now()
            mesh_marker.header.frame_id = 'synthcam_model'
            self.mesh_pub.publish(mesh_marker)


    def rasterize(self, V):
        self.cyr.set_view_matrix(np.ascontiguousarray(V))
        camv = dehomogenize(np.dot(V, homogenize(self.mesh.v).T).T)
        rgb_img, float_img, mask = self.cyr.rasterize(self.mesh.v, self.mesh.f, self.color_img, self.mesh.vt, camv)
        return rgb_img, float_img, mask


    def loop(self):
        while not rospy.is_shutdown():
            V = self.get_view_matrix()
            if V is None:
                continue
            rgb_img, float_img, mask = self.rasterize(V)
            self.publish_img(rgb_img, float_img, mask)
            self.publish_mesh()
            self.rate.sleep()

if __name__ == '__main__':
    node = Node()
    node.loop()
