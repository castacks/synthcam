import numpy as np

from semantic_segmentation_msgs.msg import LabelsTensor
from std_msgs.msg import UInt8MultiArray
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import MultiArrayDimension

MULTIARRAY_TO_DTYPE = {
        UInt8MultiArray : np.dtype('uint8'),
        Float32MultiArray : np.dtype('float32'),
        }

DTYPE_TO_MULTIARRAY = dict([(v,k) for (k,v) in MULTIARRAY_TO_DTYPE.items()])

def ndarray_to_multiarray(arr, labels=None):
    """ python ndarray to multiarray msg.
    """
    if labels is not None and len(labels) != arr.ndim:
        raise ValueError('length of labels should equal arr.ndim')
    MultiArrayType = DTYPE_TO_MULTIARRAY[arr.dtype]
    msg = MultiArrayType()
    # NOTE: ensure contiguous array *before* using strides
    arr = np.ascontiguousarray(arr)
    for di in xrange(arr.ndim):
        lbl = 'dim%d'%di if (labels is None) else labels[di]
        msg.layout.dim.append(MultiArrayDimension(label=lbl, size=arr.shape[di], stride=arr.strides[di]))
    msg.data = np.ascontiguousarray(arr).tobytes()
    return msg


def remap_labels(label_img, label_mapping):
    label_tab = np.empty((np.max(label_mapping.keys())+1), dtype='int64')
    label_tab.fill(-1)
    for lbl_src, lbl_dst in label_mapping.items():
        label_tab[lbl_src] = lbl_dst
    #import ipdb; ipdb.set_trace()
    return label_tab[label_img]


