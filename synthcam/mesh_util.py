
import os
import numpy as np
from collections import namedtuple
import cyassimp
from collections import OrderedDict

from visualization_msgs.msg import Marker
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import Point
from geometry_msgs.msg import Pose


MeshTuple = namedtuple('mesh', ['v', 'f', 'vc', 'vt', 'texture'])


def make_mesh_marker(mesh, color=None):
    """ quick hack to visualize meshes
    """
    marker = Marker()
    marker.type = Marker.TRIANGLE_LIST
    marker.action = Marker.ADD
    marker.pose = Pose()
    marker.pose.orientation.w = 1.
    if color is not None:
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]
        marker.color.a = 1.0
    marker.color.a = 1.0
    marker.scale.x = 1.
    marker.scale.y = 1.
    marker.scale.z = 1.
    marker.ns = 'marker'
    marker.id = 1
    marker.points = []
    for face in mesh.f:
        marker.points.append(Point(x=mesh.v[face[0]][0], y=mesh.v[face[0]][1], z=mesh.v[face[0]][2]))
        marker.points.append(Point(x=mesh.v[face[1]][0], y=mesh.v[face[1]][1], z=mesh.v[face[1]][2]))
        marker.points.append(Point(x=mesh.v[face[2]][0], y=mesh.v[face[2]][1], z=mesh.v[face[2]][2]))
    if mesh.vc is not None:
        for face in mesh.f:
            marker.colors.append(ColorRGBA(r=mesh.vc[face[0]][0], g=mesh.vc[face[0]][1], b=mesh.vc[face[0]][2], a=1.))
            marker.colors.append(ColorRGBA(r=mesh.vc[face[1]][0], g=mesh.vc[face[1]][1], b=mesh.vc[face[1]][2], a=1.))
            marker.colors.append(ColorRGBA(r=mesh.vc[face[2]][0], g=mesh.vc[face[2]][1], b=mesh.vc[face[2]][2], a=1.))
    return marker


def concat_meshes(meshes):
    """ merge meshes into one
    """
    v = np.concatenate([m.v for m in meshes], 0)
    f = []
    vertex_offset = 0
    for m in meshes:
        f.append(m.f + vertex_offset)
        vertex_offset += len(m.v)
    f = np.concatenate(f, 0)
    # TODO verify all or none have vc/vt
    if meshes[0].vc is not None:
        vc = np.concatenate([m.vc for m in meshes], 0)
    else:
        vc = None
    if meshes[0].vt is not None:
        vt = np.concatenate([m.vt for m in meshes], 0)
    else:
        vt = None
    mesh = MeshTuple(v=v, f=f, vc=vc, vt=vt, texture=None)
    return mesh


def load_bare_trimesh(mesh_fname):
    """ load vertex-face info with assimp
    """
    importer = cyassimp.AIImporter(str(mesh_fname))
    importer.build_scene()
    if importer.n_meshes == 0:
        raise IOError('no meshes found in {}'.format(mesh_fname))
    if importer.n_meshes > 1:
        raise IOError('only one mesh per file at the moment')
    mesh = importer.meshes[0]
    mesh_tup = MeshTuple(v=mesh.points, f=mesh.trilist, vc=None, vt=None, texture=None)
    return mesh_tup


def build_colored_scene(base_dir, mesh_fnames, mesh_colors, mesh_classify=None):
    """ make mesh with colors
    """
    if len(mesh_fnames)!=len(mesh_colors):
        raise ValueError('mesh_fnames must be same length as mesh_colors')
    if (mesh_classify is not None) and len(mesh_fnames)!=len(mesh_colors):
        raise ValueError('mesh_classify must be same length as mesh_colors')

    assert(len(mesh_fnames)==len(mesh_colors))
    rgb_to_label = OrderedDict()

    if mesh_classify is None:
        # assume mesh_classify true for all meshes
        mesh_classify = np.arange(len(mesh_fnames))+1

    meshes = []
    for mesh_fname in mesh_fnames:
        meshes.append(load_bare_trimesh(os.path.join(base_dir, mesh_fname)))
    n_colors = len(meshes)
    color_img = np.ones((n_colors, 1, 3), dtype='f4')
    for ix,col,clf in zip(range(len(mesh_colors)),
                          mesh_colors,
                          mesh_classify):
        col = np.asarray(col, dtype='u1')
        assert(col.max() <= 255)
        assert(col.min() >= 0)
        color_img[ix, 0] = col/255.
        # TODO figure out exactly wanted semantics re: unknown/0th class
        if clf != 0:
            rgb_to_label[tuple(col)] = clf


    for ix, mesh in enumerate(meshes):
        new_vt = np.array([[0.5, (n_colors-(ix+.5))/n_colors]]*len(mesh.v), dtype='f4')
        new_vc = np.array([mesh_colors[ix]]*len(mesh.v), dtype='f4')/255.
        meshes[ix] = MeshTuple(v=mesh.v, f=mesh.f, vc=new_vc, vt=new_vt, texture=mesh.texture)
    cmesh = concat_meshes(meshes)
    return cmesh, color_img, rgb_to_label
