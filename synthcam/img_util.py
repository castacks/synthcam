
import numpy as np


def rgb_image_to_label_image(img, mapping, default=0):
    """
    img: HxWx3 uint8 image
    mapping: dictionary of (r,g,b) : int
    default: label to use for keys not in mapping
    """
    if img.ndim != 3:
        raise ValueError('img must be HxWx3 matrix')
    if img.shape[2] != 3:
        raise ValueError('img must be HxWx3 matrix')

    img2 = img.astype('uint32')
    imgh = np.bitwise_or(np.bitwise_or((img2[:,:,0]<<16), (img2[:,:,1]<<8)), (img2[:,:,2]))
    # 16777216 == 255*(2**16) + 255*(2**8) + 255 + 1 == 256**3
    # TODO surprisingly, allocating this big matrix doesn't seem to impact
    # performance that much
    rgb_tab = np.zeros(16777216, dtype='uint32')
    if default != 0:
        rgb_tab.fill(default)
    for k,v in mapping.items():
        hk = k[0]*(2**16) + k[1]*(2**8) + k[2]
        rgb_tab[hk] = v
    #limg2 = rgb_tab[imgh]
    limg2 = np.take(rgb_tab, imgh)
    return limg2


def rgb_image_to_onehot_image(img, mapping, num_labels):
    img2 = img.astype('uint32')
    imgh = np.bitwise_or(np.bitwise_or((img2[:,:,0]<<16), (img2[:,:,1]<<8)), (img2[:,:,2]))
    # 16777216 == 255*(2**16) + 255*(2**8) + 255 + 1 == 256**3
    rgb_tab = np.zeros((16777216, num_labels+1), dtype='uint32')
    # explicitly add 0 entry, st (0, 0, 0) -> 0 (one-hot encoded)
    # TODO configurable
    rgb_tab[0, 0] = 1
    for k,v in mapping.items():
        hk = k[0]*(2**16) + k[1]*(2**8) + k[2]
        rgb_tab[hk, v] = 1
    limg2 = rgb_tab[imgh]
    # c01 ordering
    limg2 = limg2.transpose((2, 0, 1))
    return limg2
