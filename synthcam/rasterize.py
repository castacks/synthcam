
from collections import namedtuple
import numpy as np
from spath import Path
import cyrasterize
import rospkg

from .camera_util import homogenize, dehomogenize

rospack = rospkg.RosPack()
shader_path = Path(rospack.get_path('synthcam'))/'shaders'

texture_shaders = {'vertex':(shader_path/'texture_shader.vert').bytes(),
                   'fragment':(shader_path/'texture_shader.frag').bytes()}
normals_shaders = {'vertex':(shader_path/'normals.vert').bytes(),
                   'fragment':(shader_path/'normals.frag').bytes()}

def rasterize_texture(mesh, width, height, P, V):

    cyr = cyrasterize.CyRasterizer(width=width, height=height)
    cyr.set_shaders(**texture_shaders)
    cyr.set_projection_matrix(P)
    cyr.set_view_matrix(V)

    # coords in cam space
    # TODO flip around xy? shouldn't matter
    camv = np.dot(V, homogenize(mesh.v).T).T
    camv = dehomogenize(camv)

    rn, rnf, mask = cyr.rasterize(mesh.v, mesh.f, mesh.texture, mesh.vt, camv)
    return (rn, rnf, mask)


def rasterize_depth(mesh, width, height, P, V):
    """ note: if you need to rasterize depth+texture, use rasterize_texture
    """
    cyr = cyrasterize.CyRasterizer(width=width, height=height)
    cyr.set_shaders(**texture_shaders)
    cyr.set_projection_matrix(P)
    cyr.set_view_matrix(V)

    # coords in cam space
    # TODO flip around xy? shouldn't matter
    camv = np.dot(V, homogenize(mesh.v).T).T
    camv = dehomogenize(camv)

    # dummy texture
    tex = np.ones((1,1,3), dtype='f4')
    vt = np.zeros((mesh.v.shape[0], 2), dtype='f4')

    rn, rnf, mask = cyr.rasterize(mesh.v, mesh.f, tex, vt, camv)
    return (rn, rnf, mask)


class Rasterizer(object):

    def __init__(self, width, height, shader='texture'):
        self.cyr = cyrasterize.CyRasterizer(width, height)
        self.shader = shader
        self.flipX = True
        if shader=='texture':
            self.cyr.set_shaders(**texture_shaders)
        elif shader=='normals':
            self.cyr.set_shaders(**normals_shaders)
        else:
            raise ValueError('shaders is texture or normals')


    def set_P(self, P):
        self.cyr.set_projection_matrix(P)


    def set_V(self, V):
        self. V = V
        self.cyr.set_view_matrix(V)


    def set_mesh(self, mesh):
        self.mesh = mesh


    def rasterize_texture(self, mesh=None, V=None):
        if V is not None:
            self.set_V(V)
        if mesh is not None:
            self.set_mesh(mesh)
        if self.shader!='texture':
            self.cyr.set_shaders(**texture_shaders)

        (rn, rnf, mask) = self.cyr.rasterize(self.mesh.v, self.mesh.f, self.mesh.texture, self.mesh.vt)

        return (rn, mask)


    def rasterize_depth(self, mesh=None, V=None):
        if V is not None:
            self.set_V(V)
        if mesh is not None:
            self.set_mesh(mesh)
        if self.shader!='texture':
            self.cyr.set_shaders(**texture_shaders)
        # coords in cam space
        # TODO flip around xy? shouldn't matter
        V = self.cyr.view_matrix
        camv = np.dot(self.V, homogenize(self.mesh.v).T).T
        camv = dehomogenize(camv)
        # dummy texture if no texture exists
        tex = np.ones((1,1,3), dtype='f4')
        vt = np.zeros((self.mesh.v.shape[0], 2), dtype='f4')
        (rn, rnf, mask) = self.cyr.rasterize(self.mesh.v, self.mesh.f, tex, vt, camv)
        return (rnf, mask)


    def rasterize_normals(self, mesh=None, V=None):
        if V is not None:
            self.set_V(V)
        if mesh is not None:
            self.set_mesh(mesh)
        if self.shader!='normals':
            self.cyr.set_shaders(**normals_shaders)
        # TODO ignoring model matrix
        #MV = np.linalg.inv(np.dot(cyr.view_matrix, cyr.model_matrix))
        # transpose inverse
        invMV = np.linalg.inv(self.V)
        N = np.ascontiguousarray(invMV[:3,:3].T, dtype='f4')
        if self.flipX:
            R = np.eye(3, dtype='f4')
            R[1,1] = R[2,2] = -1.
            # flip around x to match photoscan
            fN = np.dot(R, N)
        else:
            fN = N
        tex = np.ones((1,1,3), dtype='f4')
        vt = np.zeros((self.mesh.v.shape[0], 2), dtype='f4')
        (rn, rnf, mask) = self.cyr.rasterize(self.mesh.v, self.mesh.f, tex, vt)

        normals = np.dot(fN, rn.reshape((-1, 3)).T).T
        rn2 = normals.reshape(rn.shape)
        rn2[~mask] = 0.
        #rn3 = (((rn2 + 1.0)/2.)*255).clip(0, 255).astype('u1')
        return (rn2, mask)
