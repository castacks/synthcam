
from collections import namedtuple
import numpy as np
import tf.transformations as tftf

import rospy
from geometry_msgs.msg import TransformStamped
from sensor_msgs.msg import Image, CameraInfo


def homogenize(v, value=1):
    v = np.asanyarray(v)
    return np.insert(v, v.shape[-1], value, axis=-1)


def dehomogenize(a):
    a = np.asfarray(a)
    return a[...,:-1]/a[...,np.newaxis,-1]


class CvIntrinsics(object):

    def __init__(self, cfg):
        self.width = cfg['width']
        self.height = cfg['height']
        self.calib_width = cfg.get('calib_width', cfg['width'])
        self.calib_height = cfg.get('calib_height', cfg['height'])
        self.zfar = cfg.get('zfar', 1.)
        self.znear = cfg.get('znear', 1000.)
        self.cx = cfg['cx']
        self.cy = cfg['cy']
        self.fx = cfg['fx']
        self.fy = cfg['fy']
        # TODO
        self.x0 = self.y0 = 0
        self.y_up = cfg.get('y_up', True)
        # distortion. photoscan for now
        self.k1 = cfg.get('k1', 0.)
        self.k2 = cfg.get('k2', 0.)
        self.k3 = cfg.get('k3', 0.)
        self.k4 = cfg.get('k4', 0.)
        self.p1 = cfg.get('p1', 0.)
        self.p2 = cfg.get('p2', 0.)


        # scale parameters
        # for now assuming fx = fy and cropping out left-right (thus setting cx = cy)
        # TODO do this configurably
        scale = self.height/float(self.calib_height)
        # assuming isotropic scaling

        #assert(np.allclose(scale, self.width/float(self.calib_width)))
        self.sfx = scale*self.fx
        self.sfy = scale*self.fy
        self.scy = scale*self.cy
        # self.scx = scale*self.cx
        self.scx = self.scy
        self.skew = 0. #TODO vs aspect_ratio
        self.scale = scale


    def get_scaled_K(self):
        K = [[self.sfx, 0., self.scx,],
             [0., self.sfy, self.scy,],
             [0., 0., 1.]]
        return np.asarray(K)


    def get_K(self):
        K = [[self.fx, 0., self.cx,],
             [0., self.fy, self.cy,],
             [0., 0., 1.]]
        return np.asarray(K)


    def get_scaled_camera_info(self):
        camera_info = CameraInfo()
        camera_info.K = [self.sfx, 0., self.scx,
                         0., self.sfy, self.scy,
                         0., 0., 1.]
        camera_info.P = [self.sfx,0., self.scx, 0.,
                         0., self.sfy, self.scy, 0.,
                         0., 0., 1., 0.]
        camera_info.R = [1., 0., 0.,
                         0., 1., 0.,
                         0., 0., 1.]
        camera_info.width = self.width
        camera_info.height = self.height
        return camera_info


    def get_scaled_proj_matrix(self):
        """
        from https://strawlab.org/2011/11/05/augmented-reality-with-OpenGL
        TODO note the author actually y and z around in both y-up and
        y-down cases :/
        """

        K00 = self.sfx
        K11 = self.sfy
        K01 = 0. # TODO
        K02 = self.scx
        K12 = self.scy
        width = self.width
        height = self.height
        zfar = self.zfar
        znear = self.znear
        x0 = 0 # TODO
        y0 = 0 # TODO


        if self.y_up:
            # with flip
            P = [\
             [2*K00/width, -2*K01/width, (-2*K02 + width + 2*x0)/width, 0],
             [0, -2*K11/height, (-2*K12 + height + 2*y0)/height, 0],
             [0, 0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)],
             [0, 0, -1, 0]]
            # no flip
            #P = [\
            # [-2*K00/width, -2*K01/width, (-2*K02 + width + 2*x0)/width, 0],
            # [0, -2*K11/height, (-2*K12 + height + 2*y0)/height, 0],
            # [0, 0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)],
            # [0, 0, -1, 0]]

        else:
            P = [\
             [2*K00/width, -2*K01/width, (-2*K02 + width + 2*x0)/width, 0],
             [0, 2*K11/height, (2*K12 - height + 2*y0)/height, 0],
             [0, 0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)],
             [0, 0, -1, 0]]
            # no flip
            #P = [\
            # [-2*K00/width, -2*K01/width, (-2*K02 + width + 2*x0)/width, 0],
            # [0, 2*K11/height, (2*K12 - height + 2*y0)/height, 0],
            # [0, 0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)],
            # [0, 0, -1, 0]]

        P = np.asarray(P)
        return P


    def get_proj_matrix(self):
        """
        from https://strawlab.org/2011/11/05/augmented-reality-with-OpenGL
        TODO note the author actually y and z around in both y-up and
        y-down cases :/
        """

        K00 = self.fx
        K11 = self.fy
        K01 = 0.
        K02 = self.cx
        K12 = self.cy
        width = self.width
        height = self.height
        zfar = self.zfar
        znear = self.znear
        x0 = 0 # TODO
        y0 = 0 # TODO

        if self.y_up:
            # with flip
            P = [\
             [2*K00/width, -2*K01/width, (-2*K02 + width + 2*x0)/width, 0],
             [0, -2*K11/height, (-2*K12 + height + 2*y0)/height, 0],
             [0, 0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)],
             [0, 0, -1, 0]]
            # no flip
            #P = [\
            # [-2*K00/width, -2*K01/width, (-2*K02 + width + 2*x0)/width, 0],
            # [0, -2*K11/height, (-2*K12 + height + 2*y0)/height, 0],
            # [0, 0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)],
            # [0, 0, -1, 0]]

        else:
            P = [\
             [2*K00/width, -2*K01/width, (-2*K02 + width + 2*x0)/width, 0],
             [0, 2*K11/height, (2*K12 - height + 2*y0)/height, 0],
             [0, 0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)],
             [0, 0, -1, 0]]
            # no flip
            #P = [\
            # [-2*K00/width, -2*K01/width, (-2*K02 + width + 2*x0)/width, 0],
            # [0, 2*K11/height, (2*K12 - height + 2*y0)/height, 0],
            # [0, 0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)],
            # [0, 0, -1, 0]]

        P = np.asarray(P)
        return P


def look_at(position, target):
    """ TODO untested.
    """
    position = np.asarray(position, dtype=np.float)
    target = np.asarray(target, dtype=np.float)
    M = tftf.translation_matrix(-position)
    dxyz = target-position
    dx, dy, dz = dxyz/np.linalg.norm(dxyz)
    rx = 2 * np.pi - (np.arctan2(dx, dz) + np.pi)
    ry = np.arcsin(dy)
    M = M.dot(tftf.rotation_matrix(ry, [np.cos(rx), 0, np.sin(rx)]))
    M = M.dot(tftf.rotation_matrix(-rx, [0., 1., 0.]))
    return M


def create_perspective_projection_matrix_from_bounds(
    left,
    right,
    bottom,
    top,
    near,
    far,
    dtype=None
):
    """
    adapted from pyrr library

    E 0 A 0
    0 F B 0
    0 0 C D
    0 0-1 0
    """
    A = (right + left) / (right - left)
    B = (top + bottom) / (top - bottom)
    C = -(far + near) / (far - near)
    D = -2. * far * near / (far - near)
    E = 2. * near / (right - left)
    F = 2. * near / (top - bottom)

    # we return transpose of what pyrr returns
    return np.array((
        (  E, 0., A, 0.),
        ( 0.,  F, B, 0.),
        ( 0., 0., C,  D),
        ( 0., 0.,-1, 0.),
    ), dtype=dtype)


def gl_proj_matrix(fovy_deg=60., aspect_ratio=1., znear=1., zfar=400.):
    """ Creates perspective projection matrix.

    .. seealso:: http://www.opengl.org/sdk/docs/man2/xhtml/gluPerspective.xml
    .. seealso:: http://www.geeks3d.com/20090729/howto-perspective-projection-matrix-in-opengl/

    :param float fovy: field of view in y direction in degrees
    :param float aspect: aspect ratio of the view (width / height)
    :param float near: distance from the viewer to the near clipping plane (only positive)
    :param float far: distance from the viewer to the far clipping plane (only positive)
    :rtype: numpy.array
    :return: A projection matrix representing the specified perpective.

    adapted from pyrr library
    """
    ymax = znear * np.tan(fovy_deg*np.pi/360.0)
    xmax = ymax * aspect_ratio
    return create_perspective_projection_matrix_from_bounds(-xmax, xmax, -ymax, ymax, znear, zfar)


# TODO use tf2 plugin mechanism
def create_matrix44_from_tf(tf_msg):
    txyz = [getattr(tf_msg.transform.translation, n) for n in 'xyz']
    qxyzw = [getattr(tf_msg.transform.rotation, n) for n in 'xyzw']
    # T = pyrr.matrix44.create_from_translation(txyz).T
    # R = pyrr.matrix44.create_from_quaternion(qxyzw).T
    T = tftf.translation_matrix(txyz)
    R = tftf.quaternion_matrix(qxyzw)
    return np.dot(T, R)


def create_tf_from_matrix44(M, parent_id='world', child_id='camera', ts=None):
    txyz = M[:3,3]
    #qxyzw = pyrr.quaternion.create_from_matrix(M)
    qxyzw = tftf.quaternion_from_matrix(M)
    tf_msg = TransformStamped()
    tf_msg.header.stamp = rospy.Time.now() if ts is None else ts
    tf_msg.header.frame_id = parent_id
    tf_msg.child_frame_id = child_id
    tf_msg.transform.translation.x = txyz[0]
    tf_msg.transform.translation.y = txyz[1]
    tf_msg.transform.translation.z = txyz[2]
    tf_msg.transform.rotation.x = qxyzw[0]
    tf_msg.transform.rotation.y = qxyzw[1]
    tf_msg.transform.rotation.z = qxyzw[2]
    tf_msg.transform.rotation.w = qxyzw[3]
    return tf_msg
