# synthcam

## Installation

Sadly this is a bit messy right now. This worked:

1. install glfw3 from source http://www.glfw.org/docs/latest/. make sure to build shared libraries.
1. rename or symlink `libglfw.so` to `libglfw3.so` in `/usr/local/lib`
1. make sure you have a recent version of numpy - `sudo pip install numpy` or use conda.
1. make sure you have cython. `sudo pip install cython`.
1. make sure you have glew - `sudo apt-get install libglew-dev`.
1. install https://github.com/menpo/cyrasterize from source. In `setup.py` line 59, change `glfw` to `glfw3`. There
  is probably a nicer way to do this. `sudo python setup.py install`.
1. install https://github.com/menpo/cyassimp. You need to have assimp installed. I think the ubuntu apt version (with `libassimp-dev`)
  should work. If not, install assimp from source first (http://www.assimp.org). `sudo python setup.py install`.
1. install `spath.py`, try `pip install git+https://github.com/dimatura/spath.py.git`

## Try

1. use `launch/semantics_node.xml`. check that mesh directories make sense.
2. visualize with rviz setup file `rviz/scratch.rviz` 
3. note: mesh publishing in rviz is slow and serves no purpose other than debugging. disable if not needed.


## TODO

- different formats for output
- other model loading backends: vtk/pycollada/?
- allow change of model pose
- lens distortion
- topic renaming?
- bounding box output (detector sim)
- try `python-pyassimp` as an easy to install alternative to `cyassimp`
- ~~y-down projection support~~
- ~~accurate camerainfo~~
- ~~load intrinsics in opencv format (K matrix)~~
- ~~no pyimg dependency~~
- ~~output depth~~
- output normals

## License

BSD, see LICENSE
