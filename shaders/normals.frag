#version 330
#extension GL_ARB_explicit_attrib_location : require

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
//uniform mat3 normalMatrix;

smooth in vec2 tcoord;
smooth in vec3 linearMappingCoord;
smooth in vec3 normalInterp;
smooth in vec3 FragPos;

uniform vec3 lightPos;

const float shininess = 16.0;

layout(location = 0) out vec4 outputColor;
layout(location = 1) out vec3 outputLinearMapping;

void main() {

   vec3 normal = normalize(normalInterp);
   //vec3 normal2 = normalMatrix*normal;
   //outputColor = min(vec4(ambient + diffuse + specular, 1.0f), 1);
   outputColor = vec4(normal, 1.0f);

   outputLinearMapping = linearMappingCoord;
}
